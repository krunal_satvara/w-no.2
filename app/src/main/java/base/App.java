package base;

import android.app.Activity;
import android.app.Application;

import com.google.firebase.FirebaseApp;

/**
 * Created at 23-07-2018
 */
public class App extends Application  {

    private static App app = null;
    public static Activity activity;
    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }

    public static App getAppContext() {
        return app;
    }
}
