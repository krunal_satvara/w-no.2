package com.wno2;

import android.os.Bundle;
import android.os.Handler;

import com.wno2.databinding.ActivityMainBinding;

import base.BaseActivity;

public class MainActivity extends BaseActivity<ActivityMainBinding> {

    ActivityMainBinding mainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = getViewDataBinding();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        },5000);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

}
