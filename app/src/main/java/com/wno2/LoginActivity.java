package com.wno2;

import androidx.appcompat.app.AppCompatActivity;
import base.BaseActivity;

import android.os.Bundle;

import com.wno2.databinding.ActivityLoginBinding;

public class LoginActivity extends BaseActivity<ActivityLoginBinding> {

    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = getViewDataBinding();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }
}
