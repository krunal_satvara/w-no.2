/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.wno2.MainActivity;
import com.wno2.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import retrofit.ApiUtils;
import utils.Constants;
import utils.Prefs;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    /*New Code*/
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    public static NotificationManager notificationManager;
    JSONObject jsonMapData;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    int notification_type;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Prefs.getInstance().save(Prefs.FIREBASETOKEN, s);
        Log.e("FirebaseToken ", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        if (remoteMessage.getData().size() > 0) {
            try {
                Map data = remoteMessage.getData();
                jsonMapData = new JSONObject(data);

                sendNotification(data);
            } catch (Exception e) {
                Log.w(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void sendNotification(Map<String, String> data) {
        Log.d("Notification", String.valueOf(data));
        String body = data.get(Constants.NOTIFICATION_BODY);
        String title = data.get(Constants.NOTIFICATION_TITLE);

        try {
            notification_type = Integer.parseInt(data.get(ApiUtils.NOTIFICATION_TYPE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent();
        showpush(intent, body, title);

    }

    public void showpush(Intent intent, String messageBody, String title) {
        int requestCode = (int) System.currentTimeMillis();

        PendingIntent pendingIntent = null;
        if (intent != null) {
            pendingIntent
                    = PendingIntent.getActivity(this, requestCode, intent, 0);
        } else {
            intent = new Intent(getApplicationContext(), MainActivity.class);
            pendingIntent
                    = PendingIntent.getActivity(this, requestCode, intent, 0);
        }
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentText(messageBody + "")
                .setSound(sound)
                .setAutoCancel(true)
                .setChannelId(NOTIFICATION_CHANNEL_ID);
        if (pendingIntent != null) {
            noBuilder.setContentIntent(pendingIntent);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(requestCode, noBuilder.build());
    }

    @RequiresApi(26)
    public void createChannel() {
        try {
            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.createNotificationChannel(mChannel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
