package utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.google.firebase.BuildConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;

//import com.amazonaws.util.IOUtils;

public class FileInfoUtil {

    public String getFileMimeType(ContentResolver contentResolver
            , Uri fileUri) {
        return contentResolver.getType(fileUri);
    }

    public String getFileMimeType(Uri fileUri) {
        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(fileUri
                .toString());
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                fileExtension.toLowerCase());

    }

    public String getFileExtenstion(String filePath) {
        Uri file = Uri.fromFile(new File(filePath));
        return MimeTypeMap.getFileExtensionFromUrl(file.toString());
    }

    public String getFileExtenstion(ContentResolver cr, Uri filePath) {
        ContentResolver contentResolver = cr;
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();

        // Return file Extension
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(filePath));
    }


    public String getFileName(ContentResolver contentResolver
            , Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = contentResolver.query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    public long getFileSizeMB(ContentResolver contentResolver
            , Uri uri) {
        long result = 0;
        long fileSize = 0;
        long fileSizeInMb = 0;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = contentResolver.query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getLong
                            (cursor.getColumnIndex(OpenableColumns.SIZE));
                }
            } finally {
                cursor.close();
            }
        }

        if (result != 0) {
            long fileSizeInKB = result / 1024;
            // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            fileSizeInMb = fileSizeInKB / 1024;
            fileSize = fileSizeInKB;

        }
        return fileSizeInMb;
    }


    public long getFileSize(long fileLength) {
        long result = fileLength;
        long fileSizeKB = 0;
        if (result != 0) {
            fileSizeKB = result / 1024;
            // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
        }
        return fileSizeKB;
    }


    public long getFileSize(ContentResolver contentResolver
            , Uri uri) {
        long result = 0;
        long fileSize = 0;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = contentResolver.query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getLong
                            (cursor.getColumnIndex(OpenableColumns.SIZE));
                }
            } finally {
                cursor.close();
            }
        }

        if (result != 0) {
            long fileSizeInKB = result / 1024;
            // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            fileSize = fileSizeInKB;

        }
        return fileSize;
    }


    public long getFileSizeMB(long fileLength) {
        long result = fileLength;
        long fileSizeKB = 0;
        long fileSizeInMB = 0;
        if (result != 0) {
            fileSizeKB = result / 1024;
            // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            fileSizeInMB = fileSizeKB / 1024;
        }
        return fileSizeInMB;
    }


    public String getFileType(String mimeType) {
        String fileType = "";

        if (mimeType.toString().contains("application/msword")) {
            // Word document
            fileType = Constants.OTHER_MIME;
        } else if (mimeType.toString().contains("application/pdf")) {
            // PDF file
            fileType = Constants.OTHER_MIME;
        } else if (mimeType.toString().contains("application/vnd.ms-powerpoint")) {
            // Powerpoint file
            fileType = Constants.OTHER_MIME;
        } else if (mimeType.toString().contains("application/vnd.ms-excel")) {
            // Excel file
            fileType = Constants.OTHER_MIME;
        } else if (mimeType.toString().contains("application/x-wav")) {
            // WAV audio file
            fileType = Constants.OTHER_MIME;
        } else if (mimeType.toString().contains("application/rtf")) {
            // RTF file
            fileType = Constants.OTHER_MIME;
        } else if (mimeType.toString().contains("audio/x-wav")) {
            // WAV audio file
            fileType = Constants.OTHER_MIME;

        } else if (mimeType.toString().contains("image/gif")) {
            // GIF file
            fileType = Constants.OTHER_MIME;
        } else if (mimeType.toString().contains("image/jpeg")
                || mimeType.toString().contains("jpeg")) {
            // JPG file
            fileType = Constants.IMAGE_MIME;
        } else if (mimeType.toString().contains("image/png")
                || mimeType.toString().contains("png")) {
            // JPG file
            fileType = Constants.IMAGE_MIME;
        } else if (mimeType.toString().contains("text/plain")) {
            // Text file
            fileType = Constants.OTHER_MIME;
        } else if (mimeType.toString().contains("3gp") || mimeType.toString().contains("mpg") ||
                mimeType.toString().contains("mpeg")
                || mimeType.toString().contains("mpe")
                || mimeType.toString().contains("mp4")
                || mimeType.toString().contains("avi")) {
            // Video files
            fileType = Constants.OTHER_MIME;
        } else {
            fileType = Constants.OTHER_MIME;
        }

        return fileType;
    }

    /*
     * Gets the file path of the given Uri.
     */
    /* Get uri related content real local file path. */
    public static String getPath(Context ctx, Uri uri) {
        String ret;
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                // Android OS above sdk version 19.
                ret = getUriRealPathAboveKitkat(ctx, uri);
            } else {
                // Android OS below sdk version 19
                ret = getRealPath(ctx.getContentResolver(), uri, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("DREG", "FilePath Catch: " + e);
            ret = getFilePathFromURI(ctx, uri);
        }
        return ret;
    }

    private static String getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
            String TEMP_DIR_PATH = Environment.getExternalStorageDirectory().getPath();
            File copyFile = new File(TEMP_DIR_PATH + File.separator + fileName);
            Log.d("DREG", "FilePath copyFile: " + copyFile);
            copy(context, contentUri, copyFile);
            return copyFile.getAbsolutePath();
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
//            IOUtils.copy(inputStream, outputStream); // org.apache.commons.io
            inputStream.close();
            outputStream.close();
        } catch (Exception e) { // IOException
            e.printStackTrace();
        }
    }

    //get Real File Path for content uri
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private static String getUriRealPathAboveKitkat(Context ctx, Uri uri) {
        String ret = "";

        if (ctx != null && uri != null) {

            if (isContentUri(uri)) {
                if (isGooglePhotoDoc(uri.getAuthority())) {
                    ret = uri.getLastPathSegment();
                } else {
                    ret = getRealPath(ctx.getContentResolver(), uri, null);
                }
            } else if (isFileUri(uri)) {
                ret = uri.getPath();
            } else if (isDocumentUri(ctx, uri)) {

                // Get uri related document id.
                String documentId = DocumentsContract.getDocumentId(uri);

                // Get uri authority.
                String uriAuthority = uri.getAuthority();

                if (isMediaDoc(uriAuthority)) {
                    String idArr[] = documentId.split(":");
                    if (idArr.length == 2) {
                        // First item is document type.
                        String docType = idArr[0];

                        // Second item is document real id.
                        String realDocId = idArr[1];

                        // Get content uri by document type.
                        Uri mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        if ("image".equals(docType)) {
                            mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        } else if ("video".equals(docType)) {
                            mediaContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        } else if ("audio".equals(docType)) {
                            mediaContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        }

                        // Get where clause with real document id.
                        String whereClause = MediaStore.Images.Media._ID + " = " + realDocId;

                        ret = getRealPath(ctx.getContentResolver(), mediaContentUri, whereClause);
                    }

                } else if (isDownloadDoc(uriAuthority)) {
                    // Build download uri.
                    Uri downloadUri = Uri.parse("content://downloads/public_downloads");

                    // Append download document id at uri end.
                    Uri downloadUriAppendId = ContentUris.withAppendedId(downloadUri, Long.valueOf(documentId));

                    ret = getRealPath(ctx.getContentResolver(), downloadUriAppendId, null);

                } else if (isExternalStoreDoc(uriAuthority)) {
                    String idArr[] = documentId.split(":");
                    if (idArr.length == 2) {
                        String type = idArr[0];
                        String realDocId = idArr[1];

                        if ("primary".equalsIgnoreCase(type)) {
                            ret = Environment.getExternalStorageDirectory() + "/" + realDocId;
                        }
                    }
                }
            }
        }

        return ret;
    }

    /* Check whether this uri represent a document or not. */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private static boolean isDocumentUri(Context ctx, Uri uri) {
        boolean ret = false;
        if (ctx != null && uri != null) {
            ret = DocumentsContract.isDocumentUri(ctx, uri);
        }
        return ret;
    }

    /* Check whether this uri is a content uri or not.
     *  content uri like content://media/external/images/media/1302716
     *  */
    private static boolean isContentUri(Uri uri) {
        boolean ret = false;
        if (uri != null) {
            String uriSchema = uri.getScheme();
            if ("content".equalsIgnoreCase(uriSchema)) {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this uri is a file uri or not.
     *  file uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
     * */
    private static boolean isFileUri(Uri uri) {
        boolean ret = false;
        if (uri != null) {
            String uriSchema = uri.getScheme();
            if ("file".equalsIgnoreCase(uriSchema)) {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this document is provided by ExternalStorageProvider. */
    private static boolean isExternalStoreDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.android.externalstorage.documents".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by DownloadsProvider. */
    private static boolean isDownloadDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.android.providers.downloads.documents".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by MediaProvider. */
    private static boolean isMediaDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.android.providers.media.documents".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by google photos. */
    private static boolean isGooglePhotoDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.google.android.apps.photos.content".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Return uri represented document file real local path.*/
    @SuppressLint("Recycle")
    private static String getRealPath(ContentResolver contentResolver, Uri uri, String whereClause) {
        String ret = "";

        // Query the uri with condition.
        Cursor cursor = contentResolver.query(uri, null, whereClause, null, null);

        if (cursor != null) {
            boolean moveToFirst = cursor.moveToFirst();
            if (moveToFirst) {

                // Get columns name by uri type.
                String columnName = MediaStore.Images.Media.DATA;

                if (uri == MediaStore.Images.Media.EXTERNAL_CONTENT_URI) {
                    columnName = MediaStore.Images.Media.DATA;
                } else if (uri == MediaStore.Audio.Media.EXTERNAL_CONTENT_URI) {
                    columnName = MediaStore.Audio.Media.DATA;
                } else if (uri == MediaStore.Video.Media.EXTERNAL_CONTENT_URI) {
                    columnName = MediaStore.Video.Media.DATA;
                }

                // Get column index.
                int columnIndex = cursor.getColumnIndex(columnName);

                // Get column value which is the uri related file local path.
                ret = cursor.getString(columnIndex);
            }
        }

        return ret;
    }

    //Open File in spcific app using Intent Action view.
//    https://stackoverflow.com/questions/38200282/android-os-fileuriexposedexception-file-storage-emulated-0-test-txt-exposed
    public void openFile(Activity context, File url, String fileExt) throws IOException {
        // Create URI
        File file = url;
        Uri uri = Uri.fromFile(file);

//        Intent intent = new Intent(Intent.ACTION_VIEW);
        Intent intent = new Intent();
        // Check what kind of file you are trying to open, by comparing the url with extensions.
        // When the if condition is matched, plugin sets the correct intent (mime) type,
        // so Android knew what application to use to open the file
        if (fileExt.toString().contains("doc")
                || fileExt.toString().contains("docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword");
        } else if (fileExt.toString().contains("pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf");
        } else if (fileExt.toString().contains("ppt")
                || fileExt.toString().contains("pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if (fileExt.toString().contains("xls")
                || fileExt.toString().contains("xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if (fileExt.toString().contains("zip")
                || fileExt.toString().contains("rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav");
        } else if (fileExt.toString().contains("rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf");
        } else if (fileExt.toString().contains("wav")
                || fileExt.toString().contains("mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav");
        } else if (fileExt.toString().contains("gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif");
        } else if (fileExt.toString().contains("jpg")
                || fileExt.toString().contains("jpeg")
                || fileExt.toString().contains("png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg");
        } else if (fileExt.toString().contains("txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain");
        } else if (fileExt.toString().contains("3gp")
                || fileExt.toString().contains("mpg")
                || fileExt.toString().contains("mpeg")
                || fileExt.toString().contains("mpe")
                || fileExt.toString().contains("mp4")
                || fileExt.toString().contains("avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*");
        } else {
            //if you want you can also define the intent type for any other file

            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(uri, "*/*");
        }

//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startActivity(intent);


        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String type = mime.getMimeTypeFromExtension(fileExt);
        try {

            intent.setAction(Intent.ACTION_VIEW);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Uri contentUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID, file);
                intent.setDataAndType(contentUri, type);
            } else {
                intent.setDataAndType(Uri.fromFile(file), type);
            }

            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivityForResult(intent, 1002);
        } catch (ActivityNotFoundException anfe) {
            Toast.makeText(context, "No activity found to open this attachment.", Toast.LENGTH_LONG).show();
        }
    }

    public void openUrlUsingIntent(String webUrl, Context context) {
        String url = webUrl;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }


    public boolean checkFileType(String fileExt) throws IOException {
        boolean isFileType = false;
        if (fileExt.toString().contains("doc")
                || fileExt.toString().contains("docx")) {
            // Word document
            isFileType = true;
        } else if (fileExt.toString().contains("pdf")) {
            // PDF file
            isFileType = true;
        } else if (fileExt.toString().contains("ppt")
                || fileExt.toString().contains("pptx")) {
            // Powerpoint file
            isFileType = true;
        } else if (fileExt.toString().contains("xls")
                || fileExt.toString().contains("xlsx")) {
            // Excel file
            isFileType = true;
        }else if (fileExt.toString().contains("wav")
                || fileExt.toString().contains("mp3")) {
            // WAV audio file
            isFileType = true;
        }  else if (fileExt.toString().contains("jpg")
                || fileExt.toString().contains("jpeg")
                || fileExt.toString().contains("png")) {
            // JPG file
            isFileType = true;
        } else if (fileExt.toString().contains("txt")) {
            // Text file
            isFileType = true;
        }

        return isFileType;

    }

    public String getFileName(Uri uri, ContentResolver contentResolver) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = contentResolver.query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


}
