package utils;

import com.wno2.R;

public class Constants {

    public static final String ACCESS_TOKEN = "Authorization";


    public static final String ddMMyyyy = "dd/MM/yyyy";
    public static final String EEEddMMM = "EEE. dd MMM";

    public static final int RC_SIGN_IN = 101;
    public static final String FROM = "from";
    public static final String PATH_IMAGES = R.string.app_name + "/Images";
    //Notification
    public static final String NOTIFICATION_BODY = "body";
    public static final String NOTIFICATION_TITLE = "title";
    //File Uri Mime
    public static final String OTHER_MIME = "O";
    public static final String IMAGE_MIME = "I";
    //Location Check Utils
    public static final int REQUEST_CHECK_SETTINGS = 1003;

    public static final String REAL_ACCOUNT = "Real Account";
    public static final String PERSONAL_ACCOUNT = "Personal Account";
    public static final String NOMINAL_ACCOUNT = "Nominal Account";
}
