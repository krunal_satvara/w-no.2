package utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Base64;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.LazyHeaderFactory;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.wno2.R;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;


public class ImageDisplayUitls {

    /**
     * Display image.
     *
     * @param image     the image
     * @param activity  the activity
     * @param imageView the image view
     */
    public static void displayImages(String image, Activity activity
            , ImageView imageView) {
        Glide.with(activity)
                .load(image)
                .dontAnimate()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);
    }

    public static void displayImageView(String image, Activity activity,
                                        AppCompatImageView imageView) {
        Glide.with(activity)
                .load(image)
                .into(imageView);
    }


    public static void displayImage(String image, Activity activity
            , ImageView imageView) {
        try {
            Glide.with(activity)
                    .load(image)
                    .error(R.mipmap.ic_launcher)
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void displayImage(String image, Context activity
            , ImageView imageView) {
        Glide.with(activity)
                .load(image)
                .into(imageView);
    }

    public static void displayImage(String image, Context activity
            , ImageView imageView, int imagev) {
        Glide.with(activity)
                .load(image)
                .placeholder(imagev)
                .into(imageView);
    }

    public static void displayImages(String image, Activity activity
            , ImageView imageView, int placeholder) {
        Glide.with(activity)
                .load(image)
                .placeholder(placeholder)
                .into(imageView);
    }

    public static void displayImageUri(Uri image, Activity activity
            , ImageView imageView) {
        Glide.with(activity)
                .load(image)
                .error(R.drawable.common_full_open_on_phone)
                .placeholder(R.drawable.common_full_open_on_phone)
                .into(imageView);
    }

    public static void displayImageUri(Uri image, Context activity
            , ImageView imageView) {
        Glide.with(activity)
                .load(image)
                .error(R.drawable.common_full_open_on_phone)
                .placeholder(R.drawable.common_full_open_on_phone)
                .into(imageView);
    }


    public static void displayImage(String image, Activity activity
            , ImageView imageView, int placehoder) {

//        LazyHeaders auth = new LazyHeaders.Builder() // can be cached in a field and reused
//                .addHeader("Authorization"
//                        , new BasicAuthorization(username, password))
//                .build();
//
//        Glide.with(activity)
//                .load(new GlideUrl(image, auth)) // GlideUrl is created anyway so there's no extra objects allocated
//                .error(placehoder)
//                .placeholder(placehoder)
//                .into(imageView);

        Glide.with(activity)
                .load(image) // GlideUrl is created anyway so there's no extra objects allocated
                .error(placehoder)
                .placeholder(placehoder)
                .dontAnimate()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imageView);
    }

//    public static void displayImage(String image, Activity activity
//            , CircleImageView imageView, int placehoder) {
//
////        LazyHeaders auth = new LazyHeaders.Builder() // can be cached in a field and reused
////                .addHeader("Authorization"
////                        , new BasicAuthorization(username, password))
////                .build();
////
////        Glide.with(activity)
////                .load(new GlideUrl(image, auth)) // GlideUrl is created anyway so there's no extra objects allocated
////                .error(placehoder)
////                .placeholder(placehoder)
////                .into(imageView);
//
//        Glide.with(activity)
//                .load(image) // GlideUrl is created anyway so there's no extra objects allocated
//                .error(placehoder)
//                .placeholder(placehoder)
//                .dontAnimate()
//                .listener(new RequestListener<Drawable>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                        return false;
//                    }
//                })
//                .into(imageView);
//    }

    public static void displayPngImage(String image, Activity activity
            , ImageView imageView, int placehoder) {
        Glide.with(activity)
                .load(image)
                .placeholder(placehoder)
                .into(imageView);
    }


    public class BasicAuthorization implements LazyHeaderFactory {
        private final String username;
        private final String password;

        public BasicAuthorization(String username, String password) {
            this.username = username;
            this.password = password;
        }

        @Override
        public String buildHeader() {
            return "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
        }
    }


}
