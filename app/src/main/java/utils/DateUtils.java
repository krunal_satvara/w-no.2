package utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

    public static final String DD_MM_YYYY = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DDMMYYYY = "dd-MM-yyyy";
    public static final String DD_MMM_YYYY = "dd MMM yyyy";
    public static final String DATENOTIFICATION = "dd MMM, HH:mm";

    public static int monthsBetweenDates(int year, int month, int day) {

        Calendar dob = Calendar.getInstance();
        dob.set(year, month, day);

        Calendar today = Calendar.getInstance();

        int monthsBetween = 0;
        int dateDiff = today.get(Calendar.DAY_OF_MONTH) - dob.get(Calendar.DAY_OF_MONTH);

        if (dateDiff < 0) {
            int borrrow = today.getActualMaximum(Calendar.DAY_OF_MONTH);
            dateDiff = (today.get(Calendar.DAY_OF_MONTH) + borrrow) - dob.get(Calendar.DAY_OF_MONTH);
            monthsBetween--;

            if (dateDiff > 0) {
                monthsBetween++;
            }
        } else {
            monthsBetween++;
        }
        monthsBetween += today.get(Calendar.MONTH) - dob.get(Calendar.MONTH);
        monthsBetween += (today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)) * 12;
        return monthsBetween;
    }


    public static String getConvertedDate(long millis) {
        //creating Date from millisecond
        Date currentDate = new Date(millis);
        String convertedDate = "";
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            //formatted value of current Date
            convertedDate = df.format(currentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public String convertDate(String yyMMddDate) {
        String ddMMyyDate = "";
        //Date of birth
        String eventDate = yyMMddDate;
        if (eventDate != null && !eventDate.isEmpty()) {
            try {
                //create SimpleDateFormat object with source string date format
                SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");

                //parse the string into Date object
                Date date = sdfSource.parse(eventDate);

                //create SimpleDateFormat object with desired date format
                SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MM-yyyy");

                //parse the date into another format
                eventDate = sdfDestination.format(date);

                Log.d("Date", eventDate);
                ddMMyyDate = eventDate;
            } catch (ParseException pe) {
                System.out.println("Parse Exception : " + pe);
            }
        } else {
            ddMMyyDate = "";
        }
        return ddMMyyDate;
    }

    public static String getDateddmmyyyy(String inputDate) {
        String formattedDate = "";
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd.MM.yyyy");
            Date date = inputFormat.parse(inputDate);
            formattedDate = outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String convertDateToYYYYMMSS(String datetime) {
        String convertedDate = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = (Date) formatter.parse(datetime);

            formatter.setTimeZone(TimeZone.getDefault());
            String formattedDate = formatter.format(date);

            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
            SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
            Date cdate = inputFormat.parse(formattedDate);

            convertedDate = outputFormat.format(cdate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

}