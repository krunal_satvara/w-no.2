package utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wno2.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.BuildConfig;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import okhttp3.MediaType;
import okhttp3.RequestBody;

import static android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE;
import static android.text.TextUtils.isEmpty;

/**
 * Created by Himangi Patel on 6/12/2017.
 */

public class AppUtils {

    public static void logOut(Context context) {
        Prefs.getInstance().clearAll();
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showSnackBar(Activity activity, @StringRes int mainTextString, final int actionStringId,
                                    View.OnClickListener listener) {
        Snackbar.make(activity.findViewById(android.R.id.content),
                mainTextString,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(activity.getString(actionStringId), listener).show();
    }

    public static void requestEdittextFocus(Activity activity, EditText view) {
        view.requestFocus();
//        showKeyboard(activity, view);
    }

    public static RequestBody getRequestBody(String value) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), value);
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String SHA1(String text)
            throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        byte[] sha1hash = md.digest();
        return convertToHex(sha1hash);
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte)
                        : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String getText(TextView textView) {
        return textView.getText().toString().trim();
    }

    public static boolean isEmptyText(TextView textView) {
        return isEmpty(getText(textView));
    }

    public static boolean hasInternet(Activity context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (!(networkInfo != null && networkInfo.isConnectedOrConnecting())) {
            showToast(context, context.getResources().getString(R.string.no_internet));
        }
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static boolean hasInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (!(networkInfo != null && networkInfo.isConnectedOrConnecting())) {
            showToast(context, context.getResources().getString(R.string.no_internet));
        }
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public static boolean hasInternetNoMsg(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (!(networkInfo != null && networkInfo.isConnectedOrConnecting())) {
//            showToast(context, context.getResources().getString(R.string.no_internet));
        }
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }


    public static void showKeyboard(Activity activity, EditText view) {

        Context context = activity;
        try {
            if (context != null) {
                InputMethodManager inputManager =
                        (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            }
        } catch (Exception e) {
            Logger.withTag("Exception on  show").log(e.toString());
        }
    }

    public static void hideKeyboard(Activity ctx) {
        if (ctx.getCurrentFocus() != null) {
            InputMethodManager imm =
                    (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(ctx.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static File getWorkingDirectory() {
        File directory =
                new File(Environment.getExternalStorageDirectory(), BuildConfig.APPLICATION_ID);
        if (!directory.exists()) {
            directory.mkdir();
        }
        return directory;
    }


    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp) {
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    public static void expand(final View v) {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    @SuppressLint("MissingPermission")
    public static void openCallIntent(Activity activity, String phone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        activity.startActivity(intent);
    }

    public static File saveImageToGallery(Context context, Bitmap bitmap) {
        File photo =
                new File(getWorkingDirectory().getAbsolutePath(),
                        SystemClock.currentThreadTimeMillis() + ".jpg");
        try {

            FileOutputStream fos = new FileOutputStream(photo.getPath());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
            if (photo.exists()) {
                ContentValues values = new ContentValues();

                values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
                values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                values.put(MediaStore.MediaColumns.DATA, photo.getAbsolutePath());

                context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                showToast(context, "Image saved to Gallery");
            }
            return photo;
        } catch (Exception e) {
            Log.e("Picture", "Exception in photoCallback", e);
        }
        return null;
    }

    public static Uri shareImage(Context context, Bitmap bitmap) {
        File photo =
                new File(getWorkingDirectory().getAbsolutePath(),
                        SystemClock.currentThreadTimeMillis() + ".jpg");
        try {

            FileOutputStream fos = new FileOutputStream(photo.getPath());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();
            if (photo.exists()) {
                ContentValues values = new ContentValues();

                values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
                values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                values.put(MediaStore.MediaColumns.DATA, photo.getAbsolutePath());

                return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            }
        } catch (Exception e) {
            Log.e("Picture", "Exception in photoCallback", e);
        }
        return null;
    }


    public static boolean isDigitsOnly(String number) {
        return TextUtils.isDigitsOnly(number);
    }


    public static CharSequence getSpannableText(Context context, String date) {
        String[] each = date.split(" ");

        int size12 = context.getResources().getDimensionPixelSize(R.dimen._12sdp);
        int size14 = context.getResources().getDimensionPixelSize(R.dimen._18sdp);

        SpannableString span1 = new SpannableString(each[0]);
        SpannableString span2 = new SpannableString(each[1]);

        span1.setSpan(new AbsoluteSizeSpan(size14), 0, each[0].length(), SPAN_INCLUSIVE_INCLUSIVE);
        span2.setSpan(new AbsoluteSizeSpan(size12), 0, each[1].length(), SPAN_INCLUSIVE_INCLUSIVE);

        return TextUtils.concat(span1, "\n", span2);
    }


    //get Screen with column
    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 180);
        return noOfColumns;
    }

    public static String getValidateString(String value) {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("null")) {
            return "";
        }
        return value;
    }

    public static String getValidateString(String value, String value2) {
        if (value != null || !value.isEmpty() || !value.equalsIgnoreCase("null")
                && value2 != null || !value2.isEmpty() || !value2.equalsIgnoreCase("null")) {
            return value + " " + value2;
        } else if (value != null || !value.isEmpty() || !value.equalsIgnoreCase("null")) {
            return value;
        }
        return value;
    }

    public static String getValidationOFString(String value) {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("null")) {
            return " - ";
        }
        return value;
    }

    /**
     * return formatted date GiveRating EEE dd MMM  - (Friday 20 Sep)
     *
     * @return
     */
    public static String getDateFrometDayDateMon(long miliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliseconds);
        Date c = calendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat(Constants.EEEddMMM);
        return df.format(c);
    }


    public static String getTimeInAMPM(long timestemp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestemp);
        SimpleDateFormat formatTime = new SimpleDateFormat("hh:mm aa");
        String time = formatTime.format(calendar.getTimeInMillis());
        return time;
    }

    public static String getHoursMinutesAMPMFromHoursMin(int hourOfDay, int minute) {
        Calendar datetime = Calendar.getInstance();
        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        datetime.set(Calendar.MINUTE, minute);
        String am_pm = "";
        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
            am_pm = "AM";
        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
            am_pm = "PM";

        //String strHrsToShow = (datetime.get(Calendar.HOUR) == 0) ? "12" : datetime.get(Calendar.HOUR) + "";

        return String.format("%02d", datetime.get(Calendar.HOUR)) + ":"
                + String.format("%02d", datetime.get(Calendar.MINUTE)) + " " + am_pm;
    }

    public static String getDatefromLong(long milliseconds) {
        Date currentDate = new Date(milliseconds);
        DateFormat df = new SimpleDateFormat(Constants.ddMMyyyy);
        return df.format(currentDate);
    }

    public static Calendar getCalender(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, day);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, year);
        return calendar;
    }

    public static String getTimeHHMMAMPMFromLong(long time) {
        Date dt = new Date(time);
        SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
        String formatedTime = sdfs.format(dt);
        return formatedTime;
    }


    public static DecimalFormat df = new DecimalFormat("0.00");

    public static String setPriceWithBackEuroSymbol(String typeTask
            , String budgetValue
            , Integer requireHour) {
        String euro = "\u20ac";
        String amount;
        if (typeTask.equalsIgnoreCase("h")) {
            double amt = Double.parseDouble(budgetValue);
            double dblAmt = amt
                    * requireHour;
            amount = df.format(dblAmt) + " " + euro;
        } else {
            amount = df.format(Double.parseDouble(budgetValue)) + " " + euro;
        }
        return amount;
    }

    public static String setPriceEuro(String value) {
        String euro = "\u20ac";
        String amount = "";
        try {
            double amt = Double.parseDouble(value);
            double dblAmt = amt;
            amount = euro + " " + df.format(dblAmt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return amount;
    }

    public static String setEuroToPrice(String value) {
        String euro = "\u20ac";
        String amount = "";
        try {
            double amt = Double.parseDouble(value);
            double dblAmt = amt;
            amount = euro + " " + df.format(dblAmt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return amount;
    }

    public static boolean isTaskDeleteable(String status) {
        if (status != null && !status.isEmpty() && status.equalsIgnoreCase("P")) {
            return true;
        }
        return false;
    }

    public static boolean isOfferDeleteable(String status) {
        if (status != null && !status.isEmpty() && status.equalsIgnoreCase("RE")) {
            return true;
        }
        return false;
    }

    public static void setDialogHeight(BottomSheetDialogFragment dialogFragment) {
        try {
            dialogFragment.getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    BottomSheetDialog d = (BottomSheetDialog) dialog;
                    View bottomSheetInternal = d.findViewById(R.id.design_bottom_sheet);
                    BottomSheetBehavior.from(bottomSheetInternal).setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String fulllName(String firstName, String lastName) {
        String name = "";
        if (firstName != null && !firstName.equalsIgnoreCase("")) {
            if (lastName != null && !lastName.equalsIgnoreCase("")) {
                name = firstName + " " + lastName;
            } else {
                name = firstName;
            }
        }
        return name;
    }


    /**
     * Gets fire base token.
     *
     * @param activity the activity
     */
    public static void getFirebaseToken(Activity activity) {
        try {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnSuccessListener(activity
                            , new OnSuccessListener<InstanceIdResult>() {
                                @Override
                                public void onSuccess(InstanceIdResult instanceIdResult) {
                                    String newToken = instanceIdResult.getToken();
                                    Prefs.getInstance().save(Prefs.FIREBASETOKEN, newToken + "");
                                    Log.e("Token ", newToken);
                                }
                            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Error Token", e.getMessage());
        }
    }


    /**
     * Generate Unique id - a long unique number
     *
     * @return unique id
     */
    public static String getUniqueId() {
        int min = 0;
        int max = 99;
        int random = new Random().nextInt((max - min) + 1) + min;
        return Calendar.getInstance().getTimeInMillis() + "" + random;
    }

    /**
     * Gets storage path images.
     *
     * @return storage path images
     */
    public static String getStoragePathImages() {
        return Environment.getExternalStorageDirectory() + File.separator + Constants.PATH_IMAGES;
    }


    /**
     * Checkif permission granted boolean.
     *
     * @param activity   the activity
     * @param permission the permission
     * @return the boolean
     */
    public static boolean checkifPermissionGranted(Activity activity, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    public static String[] validateRequestedPermission(Context mContent, String[] orgPermissionRequired) {

        ArrayList<String> mPermissionRequired = new ArrayList<>();
        for (int i = 0; i < orgPermissionRequired.length; i++) {
            if (!checkIfPermissionEnabled(mContent, orgPermissionRequired[i])) {
                mPermissionRequired.add(orgPermissionRequired[i]);
            }
        }
        return mPermissionRequired.toArray(new String[mPermissionRequired.size()]);
    }

    public static boolean checkIfPermissionEnabled(Context mContext, String permission) {
        if (ActivityCompat.checkSelfPermission(mContext, permission) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }



    public static void createAlertDialog(Activity activity,
                                         String message,
                                         String positiveText,
                                         String negativeText,
                                         DialogInterface.OnClickListener mPostitive,
                                         DialogInterface.OnClickListener mNegative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity,
                R.style.AppTheme))
                .setPositiveButton(positiveText, mPostitive)
                .setNegativeButton(negativeText, mNegative)
                .setMessage(message);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(ContextCompat.getColor(activity, R.color.clr_grey));
        Button pbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    public static AlertDialog createAlertDialog(Activity activity, String message,
                                                String negativeText, DialogInterface.OnClickListener mDialogClick) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity,
                R.style.AppTheme))
                .setNegativeButton(negativeText, mDialogClick)
                .setMessage(message);
        return builder.create();
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public static int getResourceId(Context context, String pVariableName, String pResourcename, String pPackageName) {
        try {
            return context.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

}
