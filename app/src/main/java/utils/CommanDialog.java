package utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Button;

import com.wno2.R;

import androidx.core.content.ContextCompat;

public class CommanDialog {

    public interface setOnClearDialogEvent {
        public void onClickYes();
    }

    public interface setOnCancleEvent {
        public void onCancelEvent();
    }

    Activity activity;
    setOnClearDialogEvent clearDialogEvent;
    setOnCancleEvent cancelEvent;

    public CommanDialog(Activity activity
            , setOnClearDialogEvent clearDialogEvent) {
        this.activity = activity;
        this.clearDialogEvent = clearDialogEvent;
    }

    public void functionSetOnCancelEvent(setOnCancleEvent cancelEvent) {
        this.cancelEvent = cancelEvent;
    }

    /*
     * @desc Zignuts 30-04-019
     * open Alert dialog of gps.
     * This alert dialog will open if gps is not available.
     */
    AlertDialog alertDialogClrLSt;

    public void buildAlertMessageClearList(String title, String message, String strPositive
            , String strNegative) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(strPositive
                        , new DialogInterface.OnClickListener() {
                            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                clearDialogEvent.onClickYes();
                            }
                        })
                .setNegativeButton(strNegative
                        , new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog
                                    , @SuppressWarnings("unused") final int id) {
                                dialog.cancel();
                                if (cancelEvent != null)
                                    cancelEvent.onCancelEvent();
                            }
                        });
        alertDialogClrLSt = builder.create();
        alertDialogClrLSt.show();

        Button nbutton = alertDialogClrLSt.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        Button pbutton = alertDialogClrLSt.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    public AlertDialog getAlertDailog() {
        return alertDialogClrLSt;
    }
}
