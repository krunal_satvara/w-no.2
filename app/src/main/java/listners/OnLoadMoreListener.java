package listners;

public interface OnLoadMoreListener {
    void onLoadMore();
}