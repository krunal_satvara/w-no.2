package interfaces;

public interface ApiResponse<T> {
    void onSuccess(retrofit2.Response response);

    void onFailure(String error);
}
