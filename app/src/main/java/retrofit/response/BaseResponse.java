package retrofit.response;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Base class for api response <br />
 */
public class BaseResponse {
    private static Gson gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create();

    private JsonElement data;
    private String authToken = null;
    private JsonObject originalResponse = null;

    public BaseResponse(JsonObject originalResponse, JsonElement data, String authToken) {
        this.originalResponse = originalResponse;
        this.data = data;
        this.authToken = authToken;
    }

    public static BaseResponse fromJson(JsonObject response) {
        JsonElement data = response.has("data") ? response.get("data") : response;
        String accessToken = "";
        return new BaseResponse(response, data, accessToken);
    }

    public JsonElement getData() {
        return data;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public JsonObject getOriginalResponse() {
        return originalResponse;
    }

    @Override
    public String toString() {
        return gson.toJson(this);
    }


}
