package retrofit;


public class ApiUtils {

    public static final String NOTIFICATION_TYPE = "notification_type";
    public static String BASE_URL = "https://fineshop.in/api/api/v1/";

    public static SOService getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(SOService.class);
    }

    public static final String ADD_TO_REMOVE_TOKEN = "add-remove-token";

    //Media Type
    public static final String MEDIATYPE = "application/json; charset=utf-8";
    //Status
    public static final int STATUS_401 = 401;
    public static final int STATUS_402 = 402;
    public static final int STATUS_200 = 200;
    public static final int STATUS_400 = 400;

}