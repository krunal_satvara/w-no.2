package retrofit;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;

import interfaces.ApiResponse;
import retrofit2.Response;
import utils.Constants;
import utils.Prefs;

/**
 * Created by ZTLAB-12 on 17-07-2019.
 */

public class CallServerApi {

    private Context context;
    private SOService soService;
    public static CallServerApi callServerApi;

    public CallServerApi(Context context) {
        this.context = context;
        this.soService = ApiUtils.getSOService();
    }

    /**
     * @return
     * @descget Default parameter - for authorize users
     */
    public HashMap<String, String> getHeaderParameter() {
        HashMap<String, String> params = new HashMap<>();
        params.put(Constants.ACCESS_TOKEN, "Bearer " +
                Prefs.getInstance().getString(Prefs.AUTH_TOKEN, ""));
        params.put("Accept", "application/json");
        Log.d("TOKEN", Prefs.getInstance().getString(Prefs.AUTH_TOKEN, ""));
        return params;
    }

    /**
     * @param context
     * @return
     * @desc instance of call server api
     */
    public static CallServerApi getInstance(Context context) {
        if (callServerApi == null) {
            callServerApi = new CallServerApi(context);
        }
        return callServerApi;
    }

    public void successResponse(Response response, ApiResponse apiResponse) {
        if (response.body() != null) {
            apiResponse.onSuccess(response);
        } else {
            apiResponse.onFailure(getErrorBody(response));
        }
    }

    public void failureResponse(ApiResponse apiResponse, Throwable t) {
        apiResponse.onFailure(t.getMessage());
    }

    /**
     * @param response
     * @return
     * @desc get  error body
     */
    private String getErrorBody(Response response) {
        Gson gson = new GsonBuilder().create();
//        models.Response mError = new models.Response();
        String error = "";
        try {
            if (response.code() == ApiUtils.STATUS_402 ||
                    response.code() == ApiUtils.STATUS_401) {
                Prefs.getInstance().clearAll();
//                ActivityUtils.launchActivityWithClearBackStack(context, LoginActivity.class);
            }

//            if (response.code() == ApiUtils.STATUS_401) {
//                Prefs.getInstance().clearAll();
//                ActivityUtils.launchActivityWithClearBackStack(context, LoginActivity.class);
//            }
//            mError = gson.fromJson(response.errorBody().string(), models.Response.class);
//            error = mError.getMessage();
        } catch (Exception e) {
            error = "";
        }
        return error;
    }


}
